package com.example.kubra.denemedeneme;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeechService;
import android.util.Log;
import android.widget.Button;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.Locale;

public class AppActivity extends Activity {

    ImageButton imageButton;
    ImageButton imageButton2;
    ImageButton imageButton3;
    ImageButton imageButton4;
    ImageButton imageButton5;
    ImageButton imageButton6;
    Button button;
    Button button2;
    TextToSpeech t1;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        t1 = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR){
                    t1.setLanguage(Locale.ENGLISH);
                }
            }
        });



        final Context context = this;

        imageButton =  (ImageButton) findViewById(R.id.imageButton);
        imageButton2 = (ImageButton) findViewById(R.id.imageButton2);
        imageButton3 = (ImageButton) findViewById(R.id.imageButton3);
        imageButton4 = (ImageButton) findViewById(R.id.imageButton4);
        imageButton5 = (ImageButton) findViewById(R.id.imageButton5);
        imageButton6 = (ImageButton) findViewById(R.id.imageButton6);
        button = (Button) findViewById((R.id.button));
        button2 = (Button) findViewById((R.id.button2));

        imageButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (v.getId() == R.id.imageButton) {
                    String toSpeak = "I feel hungry";
                    Toast.makeText(getApplicationContext(),toSpeak,Toast.LENGTH_SHORT).show();
                    t1.speak(toSpeak,TextToSpeech.QUEUE_FLUSH,null);
                  /*  Intent intent1 = new Intent(context, MainActivity.class);
                    startActivity(intent1);
                    */

                }
            }
        });

        imageButton2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (v.getId() == R.id.imageButton2) {
                    String toSpeak = "I am thirsty";
                    Toast.makeText(getApplicationContext(),toSpeak,Toast.LENGTH_SHORT).show();
                    t1.speak(toSpeak,TextToSpeech.QUEUE_FLUSH,null);
                   /* Intent intent2 = new Intent(context, MainActivity.class);
                    startActivity(intent2);
                    */

                }
            }
        });

        imageButton3.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (v.getId() == R.id.imageButton3) {
                    String toSpeak = "I feel cold";
                    Toast.makeText(getApplicationContext(),toSpeak,Toast.LENGTH_SHORT).show();
                    t1.speak(toSpeak,TextToSpeech.QUEUE_FLUSH,null);
                    /*Intent intent3 = new Intent(context, MainActivity.class);
                    startActivity(intent3);
                    */

                }
            }
        });

        imageButton4.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (v.getId() == R.id.imageButton4) {
                    String toSpeak = "I feel hot";
                    Toast.makeText(getApplicationContext(),toSpeak,Toast.LENGTH_SHORT).show();
                    t1.speak(toSpeak,TextToSpeech.QUEUE_FLUSH,null);
                   /* Intent intent4 = new Intent(context, MainActivity.class);
                    startActivity(intent4);
                    */


                }
            }
        });

        imageButton5.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (v.getId() == R.id.imageButton5) {
                    String toSpeak = "I need to toilet";
                    Toast.makeText(getApplicationContext(),toSpeak,Toast.LENGTH_SHORT).show();
                    t1.speak(toSpeak,TextToSpeech.QUEUE_FLUSH,null);
                    /*Intent intent5 = new Intent(context, MainActivity.class);
                    startActivity(intent5);
                    */


                }
            }
        });


        imageButton6.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                if (arg0.getId() == R.id.imageButton6) {
                    Intent intent = new Intent(context, App2Activity.class);
                    startActivity(intent);
                }
            }
        });

        /*
        // Ana menuden kamerayı açma
        button2.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if (v.getId() == R.id.button2) {
                    Intent intent6 = new Intent(context, MainActivity.class);
                    startActivity(intent6);
                }
            }
        });
        */

    }
}




